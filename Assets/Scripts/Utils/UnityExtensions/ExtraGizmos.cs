using UnityEngine;

namespace Assets.Scripts.Utils.UnityExtensions
{
    public static class ExtraGizmos
    {
        public static void DrawArrow(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.2f, float arrowHeadAngle = 20.0f)
        {
            //arrow shaft
            Gizmos.DrawRay(pos, direction);

            if (direction != Vector3.zero)
            {
                //arrow head
                Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
                Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
                Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
                Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
            }
        }
    }
}
