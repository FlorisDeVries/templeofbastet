using UnityEngine;

namespace Assets.Scripts.Utils.UnityExtensions
{
    public enum UnityLayers
    {
        Default = 0,
        TransparentFX = 1,
        IngoreRaycast = 2,
        VFX = 3,
        Water = 4,
        UI = 5,
        Traps = 9,
        Enemies = 10,
        Player = 11,
        Objectives = 12,
        Projectiles = 13,
        PlayerAttacks = 30,
        EnemyAttacks = 31
    }
}
