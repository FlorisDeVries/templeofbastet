using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RectTransformExtension
{

    /// <summary>
    /// Sets the width of a RectTransform
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="width"></param>
    /// <returns></returns>
    public static RectTransform SetWidth(this RectTransform transform, float width)
    {
        var sizeDelta = transform.sizeDelta;
        sizeDelta.x = width;
        transform.sizeDelta = sizeDelta;
        return transform;
    }
}
