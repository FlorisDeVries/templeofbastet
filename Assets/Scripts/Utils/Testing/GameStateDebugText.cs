using System;
using Assets.Scripts.Gameplay;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.DebugElements
{
    public class GameStateDebugText : MonoBehaviour
    {
        [Header("ScriptableObjects")]
        [SerializeField] private GameplayManagerSO _gameplayManager = default;

        [Header("UI Elements")]
        public TMP_Text debugUiText;

        private void OnEnable()
        {
            _gameplayManager.GameStateChangeEvent += OnNewGameState;
            debugUiText.text = _gameplayManager.CurrentGameState.ToString();
        }

        private void OnDisable()
        {
            _gameplayManager.GameStateChangeEvent -= OnNewGameState;
        }

        private void OnNewGameState(GameState newGameState)
        {
            debugUiText.text = newGameState.ToString();
        }
    }
}
