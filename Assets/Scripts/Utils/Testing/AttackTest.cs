using System;
using Assets.Scripts.Gameplay;
using Assets.Scripts.Gameplay.Combat;
using Assets.Scripts.Utils.UnityExtensions;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Utils.Testing
{
    public class AttackTest : MonoBehaviour
    {
        [SerializeField] private Vector3 _aimDirection = Vector3.forward;
        [SerializeField] private AbilitySO _ability = default;
        [SerializeField] private Faction _faction = Faction.Player; 

        [Button("Fire Attack")]
        private void FireAttack()
        {
            Instantiate(_ability.AttackPrefab, transform.position, Quaternion.Euler(_aimDirection))
                .GetInterface<IAttack>().SetupAttack(_faction, _ability);
        }

        private void OnDrawGizmosSelected()
        {
            ExtraGizmos.DrawArrow(transform.position, _aimDirection);
        }
    }
}
