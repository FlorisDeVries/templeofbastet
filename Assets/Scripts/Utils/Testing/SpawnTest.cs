﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Utils.Testing
{
    public class SpawnTest : MonoBehaviour
    {
        [SerializeField] private GameObject _prefab = default;

        [Button("Spawn")]
        private void Spawn()
        {
            Instantiate(_prefab, transform.position, Quaternion.identity);
        }
    }
}