using System;
using UnityEngine;

namespace Assets.Scripts.Utils.Testing
{
    [ExecuteInEditMode]
    public class PerlinNoise : MonoBehaviour
    {
        [SerializeField] private int _width = 40;
        [SerializeField] private int _height = 40;
        [SerializeField] private float _scale = 20;
        [SerializeField] private float _cutoff = .5f;

        private void OnEnable()
        {
            var renderer = GetComponent<Renderer>();
            renderer.sharedMaterial.mainTexture = GenerateNoise();
        }

        private void OnValidate()
        {
            var renderer = GetComponent<Renderer>();
            renderer.sharedMaterial.mainTexture = GenerateNoise();
        }

        private Texture2D GenerateNoise()
        {
            var text = new Texture2D(_width, _height);

            // Fill texture with Perlin noise
            for (int x = 0; x < _width; x++)
                for (int y = 0; y < _height; y++)
                {
                    text.SetPixel(x, y, GetColor(x, y));
                }

            text.Apply();
            return text;
        }

        private Color GetColor(int x, int y)
        {
            var noiseSample = Mathf.PerlinNoise((float)x / _width * _scale, (float)y / _height * _scale);
            if (noiseSample > _cutoff)
            {
                return Color.red;
            }
            else
            {
                return Color.blue;
            }
        }
    }
}
