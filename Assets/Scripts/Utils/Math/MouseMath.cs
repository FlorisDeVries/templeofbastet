using UnityEngine;

namespace Assets.Scripts.Utils.Math
{
    public static class MouseMath
    {
        public static Vector3 MouseWorldPositionYPos(Vector2 screenPos, float height)
        {
            var plane = new Plane(Vector3.up, new Vector3(0, height, 0));

            if(UnityEngine.Camera.main == null)
                return Vector3.zero;

            var ray = UnityEngine.Camera.main.ScreenPointToRay(screenPos);
            float dist = 0;
            if (plane.Raycast(ray, out dist))
            {
                return ray.GetPoint(dist);
            }

            return Vector3.one;
        }

        public static Vector3 MouseWorldPosition(Vector2 screenPos, LayerMask mask)
        {
            if (UnityEngine.Camera.main == null)
                return Vector3.zero;

            var ray = UnityEngine.Camera.main.ScreenPointToRay(screenPos);
            return Physics.Raycast(ray, out var hit, 500, mask) ? hit.point : MouseWorldPositionYPos(screenPos, 0);
        }
    }
}
