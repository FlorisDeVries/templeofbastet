using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public class EnableRandomChild : MonoBehaviour
    {
        private void OnEnable()
        {
            PickOne();
        }

        private void PickOne()
        {
            var random = Random.Range(0, transform.childCount);
            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                child.gameObject.SetActive(i == random);
            }
        }
    }
}
