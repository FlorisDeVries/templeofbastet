using Assets.Scripts.GridExtensions;
using UnityEngine;

namespace Assets.Scripts.Environment.Temple
{
    [ExecuteInEditMode]
    public class BasePillar : ABrushable
    {
        protected override void SetupTile()
        {
            tileType = TileType.Pillar;

            var random = 0;
            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                child.gameObject.SetActive(i == random);
            }
        }
    }
}
