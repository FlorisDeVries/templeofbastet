using System;
using System.Collections.Generic;
using Assets.Scripts.GridExtensions;
using Assets.Scripts.Utils.UnityExtensions;
using UnityEngine;

namespace Assets.Scripts.Environment.Temple
{
    public enum StepType
    {
        None,
        Straight,
        UpperCorner,
        LowerCorner
    }

    public class BaseSteps : ABrushable
    {
        [SerializeField] private GameObject _steps = default;
        [SerializeField] private GameObject _upperCorner = default;
        [SerializeField] private GameObject _lowerCorner = default;

        // Grid parameters for determining pattern
        private StepType _type = StepType.None;
        private StepType _prevType = StepType.None;

        // Lists to keep track of neighbours, separating steps from floor tiles, since these should have priority with orientation
        private List<Vector2Int> _sameSteps = new List<Vector2Int>();
        private List<Vector2Int> _upperSteps = new List<Vector2Int>();
        private List<Vector2Int> _lowerSteps = new List<Vector2Int>();

        private List<Vector2Int> _sameFloor = new List<Vector2Int>();
        private List<Vector2Int> _upperFloor = new List<Vector2Int>();

        private List<Vector2Int> _unreachable = new List<Vector2Int>();


        protected override void SetupTile()
        {
            tileType = TileType.Step;

            // Extract relevant neighbour information required to determine the type of step
            _sameSteps = new List<Vector2Int>();
            _upperSteps = new List<Vector2Int>();
            _lowerSteps = new List<Vector2Int>();

            _sameFloor = new List<Vector2Int>();
            _upperFloor = new List<Vector2Int>();

            _unreachable = new List<Vector2Int>();

            foreach (var neighbour in neighbours)
            {
                if (neighbour.Value.GetLayer() == layer)
                {
                    // Same layer
                    AddToLists(neighbour, ref _sameSteps, ref _sameFloor);
                }
                else if (neighbour.Value.GetLayer() == layer - 1)
                {
                    // Lower layer
                    AddToLists(neighbour, ref _lowerSteps, ref _unreachable);
                }
                else if (neighbour.Value.GetLayer() == layer + 1)
                {
                    // Upper layer
                    AddToLists(neighbour, ref _upperSteps, ref _upperFloor);
                }
            }

            DetermineOrientation();

            if (_type == _prevType) return;

            _prevType = _type;
            foreach (var neighbour in neighbours)
            {
                neighbour.Value.OnUpdate();
            }
        }

        private void AddToLists(KeyValuePair<Vector2Int, IBrushable> neighbour, ref List<Vector2Int> steps,
            ref List<Vector2Int> walkable)
        {
            switch (neighbour.Value.GetTileType())
            {
                case TileType.Floor:
                case TileType.Trap:
                    walkable.Add(neighbour.Key);
                    break;
                case TileType.Pillar:
                case TileType.Invisible:
                    _unreachable.Add(neighbour.Key);
                    break;
                case TileType.Step:
                    steps.Add(neighbour.Key);
                    break;
                default:
                    break;
            }
        }

        private void DetermineOrientation()
        {
            // Step straights
            if (_upperSteps.Count == 1 && (_lowerSteps.Count == 1 || _sameFloor.Count == 1))
            {
                SetupStraight(_upperSteps[0]);
                return;
            }

            // Floor straights
            if (_upperFloor.Count >= 1 && (_lowerSteps.Count == 1 || _sameFloor.Count == 1))
            {
                var lower = Vector2Int.zero;
                if (_lowerSteps.Count == 1)
                {
                    lower = _lowerSteps[0];
                }
                else if (_sameFloor.Count == 1)
                {
                    lower = _sameFloor[0];
                }

                var upper = Vector2Int.zero;
                foreach (var direction in _upperFloor)
                {
                    if (direction.y == -lower.y && direction.x == -lower.x)
                    {
                        upper = direction;
                    }
                }

                SetupStraight(upper);
                return;
            }

            // Step corners
            if (_sameSteps.Count == 2)
            {
                SetupUpperCorner(_sameSteps);
                return;
            }

            if (_upperSteps.Count == 2)
            {
                SetupLowerCorner(_upperSteps);
                return;
            }

            // Floor corners
            if (_upperFloor.Count == 2)
            {
                SetupLowerCorner(_upperFloor, 0);
                return;
            }

            if (_sameFloor.Count == 2)
            {
                SetupUpperCorner(_sameFloor, 180);
                return;
            }

            _type = StepType.None;
        }

        private void SetupStraight(Vector2Int upperDirection)
        {
            _steps.SetActive(true);
            _upperCorner.SetActive(false);
            _lowerCorner.SetActive(false);
            _type = StepType.Straight;

            Vector2 dir = upperDirection;
            dir = dir.Rotate(90);

            if (dir.magnitude != 0)
                transform.forward = new Vector3(dir.x, 0, dir.y);
        }

        private void SetupUpperCorner(List<Vector2Int> lowerTiles, float rotation = 0)
        {
            _steps.SetActive(false);
            _upperCorner.SetActive(true);
            _lowerCorner.SetActive(false);
            _type = StepType.UpperCorner;

            Vector2 dir = (lowerTiles[0] + lowerTiles[1]);
            dir.Normalize();
            dir = dir.Rotate(135 + rotation);

            if (dir.magnitude != 0)
                transform.forward = new Vector3(dir.x, 0, dir.y);
        }

        private void SetupLowerCorner(List<Vector2Int> upperTiles, float rotation = 0)
        {
            _steps.SetActive(false);
            _upperCorner.SetActive(false);
            _lowerCorner.SetActive(true);
            _type = StepType.LowerCorner;

            Vector2 dir = (upperTiles[0] + upperTiles[1]);
            dir.Normalize();
            dir = dir.Rotate(45 + rotation);

            if (dir.magnitude != 0)
                transform.forward = new Vector3(dir.x, 0, dir.y);
        }

        protected override void UpdateNeighbours()
        {
            base.UpdateNeighbours();
            if (_type != _prevType)
            {
                _prevType = _type;
                foreach (var neighbour in neighbours)
                {
                    neighbour.Value.OnUpdate();
                }
            }
        }
    }
}