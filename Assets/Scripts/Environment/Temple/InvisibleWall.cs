using System;
using Assets.Scripts.GridExtensions;
using UnityEngine;

namespace Assets.Scripts.Environment.Temple
{
    public class InvisibleWall : ABrushable
    {
        private BoxCollider _collider = default;

        protected override void SetupTile()
        {
            tileType = TileType.Invisible;

            _collider = GetComponent<BoxCollider>();
        }

        private void OnDrawGizmos()
        {
            if (_collider == null)
                return;

            Gizmos.color = Color.blue;
            Gizmos.DrawCube(transform.position + _collider.center, _collider.size);
        }
    }
}
