﻿using System;
using Assets.Scripts.Environment.DropIn;
using Assets.Scripts.GridExtensions;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Environment.Temple
{
    public class BaseTile : ABrushable
    {
        [SerializeField] private PerlinNoiseSO _noiseSO;
        [SerializeField] private GameObject _lightVariant;
        [SerializeField] private GameObject _darkVariant;
        
        protected override void SetupTile()
        {
            tileType = TileType.Floor;

            // Calculate the color of each tile, we want an alternating pattern
            var lightColor = _noiseSO.SampleWithCutoff(tilePosition.x, tilePosition.y);

            // Flip for positions in the negative space, so the patterns continues around 0
            if (transform.position.x < 0)
            {
                lightColor = !lightColor;
            }

            if (transform.position.z < 0)
            {
                lightColor = !lightColor;
            }

            if (lightColor)
            {
                _lightVariant.SetActive(true);
                _darkVariant.SetActive(false);
            }
            else
            {
                _lightVariant.SetActive(false);
                _darkVariant.SetActive(true);
            }
        }
    }
}
