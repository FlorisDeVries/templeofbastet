﻿using System.Collections;
using Assets.Scripts.Characters.Player;
using Assets.Scripts.Environment.DropIn;
using Assets.Scripts.Gameplay.Combat;
using Assets.Scripts.GridExtensions;
using UnityEngine;
using UnityEngine.VFX;

namespace Assets.Scripts.Environment.Traps
{
    public class SpikeTrap : ABrushable
    {
        [Header("Effects")]
        [SerializeField] private VisualEffect _smokeEffect = default;

        [Header("Trap timings")]
        [Tooltip("How much of the cycle should the trap be active")]
        [SerializeField]
        [Range(0, 1)]
        private float _activeRatio = .25f;
        [Tooltip("How many traps are present in the pattern")]
        [SerializeField]
        private int _trapPattern = 1;
        [Tooltip("How long each trap cycle should take")]
        [SerializeField]
        private float _cycleLength = 2f;

        // Run time properties
        private float _actualLength;
        private float _trapOffset = 0f;

        // How long before the actual activation a warning should be given
        private const float WarningActivation = .5f;

        [Header("Damage stat")]
        [SerializeField] private float _damage = 10f;

        private bool _disabled;
        private Spikes _spikes;
        
        protected override void SetupTile()
        {
            gameObject.isStatic = false;
            tileType = TileType.Trap;

            _disabled = false;
            _spikes = GetComponentInChildren<Spikes>();
            
            // Calculate the offset for each trap
            var delayMultiplier = (Mathf.Abs(tilePosition.x) + Mathf.Abs(tilePosition.y)) % _trapPattern;

            // Set the trap timings
            _actualLength = _cycleLength / _trapPattern;
            _trapOffset = delayMultiplier * (_actualLength / _trapPattern);

            if (WarningActivation + (_actualLength * _activeRatio) > _actualLength)
            {
                _disabled = true;
                Debug.LogError($"The timing is off, loop is shorter than active time + warning time: ActiveTime: {_actualLength * _activeRatio} | warning time: {WarningActivation} | CycleLength {_actualLength}");
            }

            StartCoroutine(InitialDelay());
        }

        private void OnDisable()
        {
            _disabled = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            other.GetComponent<IDamageable>()?.TakeDamage(_damage);
        }

        private IEnumerator InitialDelay()
        {
            if (_disabled) yield break;
            yield return new WaitForSeconds(_trapOffset);
            StartCoroutine(ActivateTrapLoop());
        }

        private IEnumerator ActivateTrapLoop()
        {
            if (_disabled) yield break;

            WarningShot();
            yield return new WaitForSeconds(WarningActivation);
            Activate();
            yield return new WaitForSeconds(_actualLength * _activeRatio);
            DeActivate();
            yield return new WaitForSeconds(_actualLength - (_actualLength * _activeRatio) - WarningActivation);
            StartCoroutine(ActivateTrapLoop());
        }

        private void WarningShot()
        {
            //if (hidden) return;

            _smokeEffect.Play();
        }

        private void Activate()
        {
            //if (hidden) return;

            _smokeEffect.Play();
            _spikes.Activate();
        }

        private void DeActivate()
        {
            _spikes.Deactivate();
        }
    }
}
