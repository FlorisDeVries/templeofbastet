using System;
using UnityEngine;

namespace Assets.Scripts.Environment.Traps
{
    public class Spikes : MonoBehaviour
    {
        [SerializeField] private float _activeSpeed;
        [SerializeField] private float _deactivateSpeed;
        [SerializeField] private float _retractionHeight;

        private Vector3 _activePosition = new Vector3();
        private Vector3 _inactivePosition = new Vector3();
        private Vector3 _targetPosition = new Vector3();
        private float _targetSpeed = 0;


        private void OnEnable()
        {
            gameObject.isStatic = false;

            var localPosition = transform.localPosition;
            _activePosition = localPosition;
            _inactivePosition = localPosition - new Vector3(0, _retractionHeight, 0);

            transform.localPosition = _inactivePosition;
            _targetSpeed = _deactivateSpeed;
        }

        public void Activate()
        {
            _targetPosition = _activePosition;
            _targetSpeed = _activeSpeed;
        }

        public void Deactivate()
        {
            _targetSpeed = _deactivateSpeed;
            _targetPosition = _inactivePosition;
        }

        private void FixedUpdate()
        {
            var closeEnough = Vector3.Distance(transform.localPosition, _targetPosition) < .01f;
            transform.localPosition = closeEnough ? _targetPosition : Vector3.Lerp(transform.localPosition, _targetPosition, _targetSpeed);
        }
    }
}
