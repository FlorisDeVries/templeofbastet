using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Environment.DropIn
{
    public interface IDropInBehaviour
    {
        void Hide();
        void ShowWithDelay(Vector3 showOrigin);
        void Show();
    }

    public abstract class ADropInBehaviour : MonoBehaviour, IDropInBehaviour
    {
        [SerializeField]
        protected DropInSettingsSO dropInSettings;

        // State managing
        protected bool hidden = false;
        protected bool dropInLerping = false;

        // Animation targets
        protected Vector3 targetScale = Vector3.one;
        protected Vector3 targetPos = Vector3.one;

        public virtual void Hide()
        {
            hidden = true;
            dropInLerping = false;

            targetPos = transform.localPosition;
            targetScale = transform.localScale;

            transform.localPosition += dropInSettings.RelativeStartPos;
            transform.localScale = dropInSettings.StartScale;
        }

        public void ShowWithDelay(Vector3 showOrigin)
        {
            var position = transform.position;
            var distX = Math.Abs(position.x - showOrigin.x);
            var distZ = Math.Abs(position.z - showOrigin.z);
            var distance = (int)(distX + distZ);

            StartCoroutine(ShowCoroutine(distance / dropInSettings.DelayMultiplier));
        }

        public IEnumerator ShowCoroutine(float delay)
        {
            yield return new WaitForSeconds(delay);

            Show();
        }

        public virtual void Show()
        {
            hidden = false;
            dropInLerping = true;
        }

        private void Update()
        {
            if (!dropInLerping)
                return;

            transform.localScale = Vector3.Lerp(transform.localScale, targetScale, dropInSettings.DropInSpeed * Time.deltaTime);
            transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, dropInSettings.DropInSpeed * Time.deltaTime);

            if ((Vector3.Distance(targetScale, transform.localScale) < .01f)) transform.localScale = targetScale;

            if ((Vector3.Distance(targetPos, transform.localPosition) < .01f)) transform.localPosition = targetPos;

            if (targetPos == transform.localPosition && targetScale == transform.localScale)
            {
                dropInLerping = false;
            }
        }
    }
}