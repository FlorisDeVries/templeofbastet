using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Environment.Traps;
using Assets.Scripts.Utils.UnityExtensions;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Environment.DropIn
{
    public class DropInRoom : MonoBehaviour
    {
        [SerializeField] [Tooltip("Whether the room should be showed by default")] private bool _visible = false;
        public bool Visible => _visible;

        private List<IDropInBehaviour> _behaviours = new List<IDropInBehaviour>();

        private void OnEnable()
        {
            _behaviours = gameObject.GetInterfacesInChildren<IDropInBehaviour>().ToList();
            if (_visible) return;

            foreach (var dropInBehaviour in _behaviours)
            {
                dropInBehaviour.Hide();
            }

        }

        public void RevealRoom(Vector3 position)
        {
            if (_visible) return;

            foreach (var dropInBehaviour in _behaviours)
            {
                dropInBehaviour.ShowWithDelay(position);
            }

            _visible = true;
        }
    }
}
