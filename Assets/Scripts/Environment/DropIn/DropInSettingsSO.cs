using UnityEngine;

namespace Assets.Scripts.Environment.DropIn
{
    [CreateAssetMenu(fileName = "DropInSettings", menuName = "Environment/DropInSettings")]
    public class DropInSettingsSO : ScriptableObject
    {
        public float DropInSpeed = 10f;
        public float DelayMultiplier = 25;

        public Vector3 StartScale = Vector3.zero;
        public Vector3 RelativeStartPos = new Vector3(0, -2f, 0);
    }
}
