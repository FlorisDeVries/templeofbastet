using UnityEngine;

namespace Assets.Scripts.Environment.DropIn
{
    public class DropInTrigger : MonoBehaviour
    {
        private DropInRoom _dropInRoom;

        private void OnEnable()
        {
            _dropInRoom = GetComponentInParent<DropInRoom>();
            if (_dropInRoom == null)
            {
                Debug.LogError("No dropIn room found in the parents");
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            _dropInRoom.RevealRoom(transform.position);
        }
    }
}
