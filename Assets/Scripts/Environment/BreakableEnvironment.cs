using Assets.Scripts.Gameplay.Combat;
using Assets.Scripts.Gameplay.Objectives;
using UnityEngine;

namespace Assets.Scripts.Environment
{
    public class BreakableEnvironment : MonoBehaviour, IDamageable
    {
        [SerializeField] private float _hp = 10;
        [SerializeField] private float _soulDrops = 3;
        [SerializeField] private SoulObjective _soulObjective = default;

        public void TakeDamage(float damage)
        {
            _hp -= damage;
            if (_hp < 0)
            {
                GetDestroyed();    
            }
        }

        private void GetDestroyed()
        {
            for (var i = 0; i < _soulDrops; i++)
            {
                _soulObjective.SpawnSouls(transform.position);
            }
            Destroy(this.gameObject);
        }
    }
}
