using System;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.VFX;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Gameplay.Objectives
{
    public class SoulDrop : MonoBehaviour
    {
        [SerializeField] private PlayerDataSO _playerData = default;
        [SerializeField] private SoulObjective _soulObjective = default;
        [SerializeField] private float _soulValue = 50;

        [SerializeField] private float _rotationSpeed = 5;
        [SerializeField] private float _collectionSpeed = 5;
        [SerializeField] private int _jumps = 1;
        [SerializeField] private LayerMask _mask = default;
        [SerializeField] private VisualEffect _effect = default;

        private int _jumpsDone = 0;
        private Rigidbody _rigidbody = default;
        private Vector2 _direction = Vector2.zero;
        private bool _pickedUp = false;

        private void OnEnable()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _direction = Random.insideUnitCircle;
            _rigidbody.velocity = new Vector3(_direction.x, 4f, _direction.y);
            _effect.Play();
        }
        
        private void FixedUpdate()
        {
            if (_pickedUp)
            {
                UpdateRotation();

                var targetPosition = transform.position + transform.forward * (_collectionSpeed * Time.fixedDeltaTime);
                _rigidbody.MovePosition(targetPosition);
                if ((transform.position - _playerData.PlayerPos).magnitude < 1f)
                {
                    _soulObjective.CollectSouls(_soulValue);
                    Destroy(this.gameObject);
                }
            }
            else
            {
                if (!(_rigidbody.velocity.y < 0)) return;
                _rigidbody.velocity += Vector3.up * (Physics.gravity.y * Time.fixedDeltaTime);

                if (_jumpsDone < _jumps && IsGrounded())
                {
                    _jumpsDone++;
                    _rigidbody.velocity = new Vector3(_direction.x, 4f, _direction.y) * .5f;
                }
                CheckPickup();
            }
        }
        private void UpdateRotation()
        {
            if (_playerData.PlayerPos == Vector3.zero)
                return;

            _rotationSpeed *= 1.05f;
            var targetDirection = _playerData.PlayerPos - transform.position;
            // If the two vectors are in opposite directions, rotate the forward slightly so the character does not get stuck on rotation
            if (transform.forward == targetDirection * -1)
                transform.forward = Quaternion.Euler(0, 5f, 0) * transform.forward;

            if (targetDirection != Vector3.zero)
                transform.forward = Vector3.MoveTowards(transform.forward, targetDirection, _rotationSpeed * Time.fixedDeltaTime);
        }

        private void CheckPickup()
        {
            if (!_playerData.InPickupRange(transform.position)) return;

            // Pickup logic
            _pickedUp = true;
            transform.forward = Vector3.up;
            _rigidbody.useGravity = false;
        }

        private bool IsGrounded()
        {
            return Physics.CheckSphere(transform.position - Vector3.up * .5f, .2f, _mask);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.position - Vector3.up * .5f, .2f);
        }
    }
}
