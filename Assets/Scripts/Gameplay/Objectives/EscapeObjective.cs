using UnityEngine;

namespace Assets.Scripts.Gameplay.Objectives
{
    public class EscapeObjective : MonoBehaviour
    {
        [Header("ScriptableObjects")]
        [SerializeField] private GameplayManagerSO _gameplayManager = default;

        private bool _disabled = true;

        private void OnEnable()
        {
            _disabled = true;
            _gameplayManager.GameStateChangeEvent += OnGameStateChanged;
        }

        private void OnDisable()
        {
            _gameplayManager.GameStateChangeEvent -= OnGameStateChanged;
        }

        private void OnGameStateChanged(GameState newState)
        {
            if (newState == GameState.Escaping)
                _disabled = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_disabled)
                return;

            _gameplayManager.EscapedObjective();
        }
    }
}
