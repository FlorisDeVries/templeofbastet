using UnityEngine;

namespace Assets.Scripts.Gameplay.Objectives
{
    public class PickupObjective : MonoBehaviour
    {
        [Header("ScriptableObjects")]
        [SerializeField] private GameplayManagerSO _gameplayManager = default;

        private void OnTriggerEnter(Collider other)
        {
            _gameplayManager.CollectedObjective();
        }
    }
}
