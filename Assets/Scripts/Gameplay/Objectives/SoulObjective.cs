using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Gameplay.Objectives
{
    [CreateAssetMenu(fileName = "SoulObjective", menuName = "Gameplay/Soul Objective")]
    public class SoulObjective : ScriptableObject
    {
        public event UnityAction<float> SoulCollectedEvent = delegate { };

        [SerializeField] private float _soulsToCollect = 500f;
        [SerializeField] private GameplayManagerSO _gameplayManager = default;
        [SerializeField] private GameObject _soulPrefab = default;

        private float _collectedSouls = 0;
        public float CollectedSouls => _collectedSouls;
        
        public void CollectSouls(float souls)
        {
            _collectedSouls += souls;
            SoulCollectedEvent.Invoke(_collectedSouls);

            if (_collectedSouls >= _soulsToCollect)
            {
                // Victory
                _gameplayManager.Victory();
            }
        }

        private void OnEnable()
        {
            Reset();
        }

        private void Reset()
        {
            _collectedSouls = 0;
        }

        public void SpawnSouls(Vector3 pos)
        {
            Instantiate(_soulPrefab, pos, Quaternion.identity);
        }
    }
}
