using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Utils.UnityExtensions;
using UnityEngine;

namespace Assets.Scripts.Gameplay
{
    public enum Faction
    {
        Player,
        Hostile,
        Neutral
    }

    public static class Factions
    {
        public static LayerMask GetAttackLayerMask(this Faction faction)
        {
            return faction switch
            {
                Faction.Player => LayerMask.GetMask($"Enemies"),
                Faction.Hostile => LayerMask.GetMask($"Player"),
                Faction.Neutral => LayerMask.GetMask($"Enemies"),
                _ => throw new ArgumentOutOfRangeException(nameof(faction), faction, null)
            };
        }

        public static int GetAttackLayer(this Faction faction)
        {
            return faction switch
            {
                Faction.Player => (int)UnityLayers.PlayerAttacks,
                Faction.Hostile => (int)UnityLayers.EnemyAttacks,
                Faction.Neutral => -1,
                _ => throw new ArgumentOutOfRangeException(nameof(faction), faction, null)
            };
        }

        public static int GetLayerIndex(this Faction faction)
        {
            return faction switch
            {
                Faction.Player => (int) UnityLayers.Player,
                Faction.Hostile => (int) UnityLayers.Enemies,
                Faction.Neutral => -1,
                _ => throw new ArgumentOutOfRangeException(nameof(faction), faction, null)
            };
        }
    }
}