using System.Collections;
using Assets.Scripts.Characters.Data;
using Assets.Scripts.Characters.Player;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using Assets.Scripts.Input;
using Assets.Scripts.Utils.UnityExtensions;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.VFX;

namespace Assets.Scripts.Gameplay.Combat
{
    public class Projectile : MonoBehaviour, IAttack
    {
        [Header("VFX")]
        [SerializeField] private VisualEffect _muzzle = default;
        [SerializeField] private VisualEffect _projectile = default;
        [SerializeField] private VisualEffect _impact = default;

        [Header("Movement")]
        [SerializeField] private float _speed = 5;
        [SerializeField] private float _rotationSpeed = 5;
        [SerializeField] private MouseDataSO _mouseData = default;
        [SerializeField] private PlayerDataSO _playerData = default;
        private Vector3 _target = Vector3.zero;
        private float _fixedY = 1;

        [Header("Combat")] 
        [SerializeField] private float _damage = 10;

        private float _lifetime = 10;
        private Rigidbody _rigidbody = default;
        private Faction _faction = default;

        private void UpdateRotation()
        {
            if(_target == Vector3.zero)
                return;

            var targetDirection = _target - transform.position;
            // If the two vectors are in opposite directions, rotate the forward slightly so the character does not get stuck on rotation
            if (transform.forward == targetDirection * -1)
                transform.forward = Quaternion.Euler(0, 5f, 0) * transform.forward;

            if (targetDirection != Vector3.zero)
                transform.forward = Vector3.MoveTowards(transform.forward, targetDirection, _rotationSpeed * Time.fixedDeltaTime);
        }

        private void FixedUpdate()
        {
            UpdateRotation();

            var targetPosition = transform.position + transform.forward * (_speed * Time.fixedDeltaTime);
            targetPosition.y = _fixedY;
            _rigidbody.MovePosition(targetPosition);

            if (Vector3.Distance(transform.position, _target) < .5f)
            {
                _target = Vector3.zero;
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            Explode();

            var damageable = other.gameObject.GetInterface<IDamageable>();
            damageable?.TakeDamage(_damage);
        }

        private void Explode()
        {
            _speed = 0;
            _projectile.Stop();
            _impact.Play();
            Destroy(this.gameObject, 1);
        }

        private IEnumerator ExplodeAfter(float delay)
        {
            yield return new WaitForSeconds(delay);
            if (_speed != 0)
                Explode();
        }

        public void SetupAttack(Faction faction, AbilitySO ability)
        {
            _faction = faction;
            gameObject.layer = _faction.GetAttackLayer();
            _target = _faction == Faction.Player ? _mouseData.WorldPos : _playerData.ThirdPersonFollowTransform.position;
            _fixedY = transform.position.y;
            _target.y = _fixedY;

            var rangedAbility = ability as RangedAbilitySO;
            if (rangedAbility == null)
            {
                Debug.LogWarning("The given ability data does not support a ranged attack!");
                Destroy(gameObject);
                return;
            }

            _rigidbody = GetComponent<Rigidbody>();

            _lifetime = rangedAbility.Lifetime;

            _muzzle.Play();
            _projectile.Play();


            transform.Rotate(transform.up, Random.Range(-45, 45));

            StartCoroutine(ExplodeAfter(_lifetime));
        }
    }
}
