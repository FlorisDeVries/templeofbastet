using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat
{
    [CreateAssetMenu(fileName = "AbilitySO", menuName = "Gameplay/Ability")]
    public class AbilitySO : ScriptableObject
    {
        [SerializeField] 
        private float _cooldown = 1;
        public float Cooldown => _cooldown;

        [SerializeField]
        private float _range = 2;
        public float Range => _range;

        [SerializeField]
        private int _attackIndex= 2;
        public int AttackIndex => _attackIndex;

        [SerializeField] 
        private GameObject _attackPrefab = default;
        public GameObject AttackPrefab => _attackPrefab;

        public virtual void DrawGizmo(Transform parent)
        {
            // Attack debug
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(parent.position + parent.forward.normalized * Range / 2, Range / 2);
        }
    }
}
