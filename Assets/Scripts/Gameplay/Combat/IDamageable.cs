using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat
{
    public interface IDamageable
    {
        void TakeDamage(float damage);
    }
}
