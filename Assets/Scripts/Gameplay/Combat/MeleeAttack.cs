using System;
using Assets.Scripts.Characters.Player;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.VFX;

namespace Assets.Scripts.Gameplay.Combat
{
    public class MeleeAttack : MonoBehaviour, IAttack
    {
        [Header("VFX")]
        [SerializeField] private VisualEffect _attackEffect = default;
        [SerializeField] private float _damage = 10;

        private Faction _faction = default;
        public float _attackRange = 2;
        
        private void DealDamage()
        {
            _attackEffect.Play();

            var mask = _faction.GetAttackLayerMask();
            var colliders = Physics.OverlapSphere(transform.position + transform.forward.normalized * _attackRange / 2, _attackRange / 2, mask);

            if (colliders.Length < 1)
                return;

            var player = colliders[0].GetComponent<IDamageable>();
            player.TakeDamage(_damage);
        }

        public void SetupAttack(Faction faction, AbilitySO ability)
        {
            _faction = faction;
            _attackRange = ability.Range;
            gameObject.layer = faction.GetAttackLayer();

            DealDamage();

            Destroy(this.gameObject, 1f);
        }

        private void OnDrawGizmosSelected()
        {
            // Attack debug
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position + transform.forward.normalized * _attackRange / 2, _attackRange / 2);
        }
    }
}
