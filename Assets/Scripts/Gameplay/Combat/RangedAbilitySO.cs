using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat
{
    [CreateAssetMenu(fileName = "RangedAbilitySO", menuName = "Gameplay/RangedAbility")]
    public class RangedAbilitySO : AbilitySO
    {
        [SerializeField] private float _lifeTime = 1f;
        public float Lifetime => _lifeTime;

        public override void DrawGizmo(Transform parent)
        {
            // Attack debug
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(parent.position, Range);
        }
    }
}
