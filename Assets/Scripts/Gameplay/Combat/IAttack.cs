using UnityEngine;

namespace Assets.Scripts.Gameplay.Combat
{
    public interface IAttack
    {
        void SetupAttack(Faction faction, AbilitySO abilityData);
    }
}
