using Assets.Scripts.Gameplay.Objectives;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Gameplay
{
    public enum GameState
    {
        Paused,
        Discovering,
        Escaping,
        Victory,
        Defeat
    }

    [CreateAssetMenu(fileName = "GameplayManagerSO", menuName = "Gameplay/GameplayManager")]
    public class GameplayManagerSO : ScriptableObject
    {
        public event UnityAction<GameState> GameStateChangeEvent = delegate { };

        private GameState _currentGameState = GameState.Paused;
        public GameState CurrentGameState => _currentGameState;
        
        private void OnEnable()
        {
            SetGameState(GameState.Discovering);
        }

        private void SetGameState(GameState newGameState)
        {
            if (newGameState == _currentGameState)
            {
                return;
            }
            
            _currentGameState = newGameState;
            GameStateChangeEvent.Invoke(_currentGameState);
        }

        public void CollectedObjective()
        {
            SetGameState(GameState.Escaping);
        }

        public void EscapedObjective()
        {
            SetGameState(GameState.Victory);
        }

        // TEMP Victory shortcut, should be two staged --> First collect objective, then escape
        public void Victory()
        {
            SetGameState(GameState.Victory);
        }

        public void GameOver()
        {
            SetGameState(GameState.Defeat);
        }
    }
}
