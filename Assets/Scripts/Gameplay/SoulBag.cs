using System;
using Assets.Scripts.Characters.Data;
using Assets.Scripts.Characters.Player;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using Assets.Scripts.Gameplay.Objectives;
using TMPro;
using UnityEngine;
using UnityEngine.VFX;

namespace Assets.Scripts.Gameplay
{
    public class SoulBag : MonoBehaviour
    {
        [SerializeField] private PlayerDataSO _playerData = default;
        [SerializeField] private SoulObjective _soulObjective = default;
        
        [SerializeField] private float _speed = 2.5f;
        [SerializeField] private float _distanceMultiplier = .5f;

        [SerializeField] private float _keepDistance = 2f;
        [SerializeField] private float _heightMultiplier = .5f;

        [SerializeField] private VisualEffect _soulEffect = default;

        private void FixedUpdate()
        {
            var yDiff = Mathf.Sin(Time.time) * Time.fixedDeltaTime * _heightMultiplier;
            var target = new Vector2(_playerData.PlayerPos.x, _playerData.PlayerPos.z);
            var current = new Vector2(transform.position.x, transform.position.z);

            var dir = target - current;
            var actualSpeed = _speed * _distanceMultiplier * (dir.magnitude - _keepDistance);
            
            var posDiff = dir.normalized * (actualSpeed * Time.fixedDeltaTime);
            transform.position += new Vector3(posDiff.x, yDiff, posDiff.y);
        }

        private void OnEnable()
        {
            if (_soulEffect == null) _soulEffect = GetComponentInChildren<VisualEffect>();

            _soulEffect.Play();
            _soulEffect.SetFloat("Size", 1f);
            _soulEffect.SetFloat("Count", 0);

            _soulObjective.SoulCollectedEvent += OnSoulCollected;
        }

        private void OnSoulCollected(float newSoulAmount)
        {
            _soulEffect.SetFloat("Count", newSoulAmount);
        }
    }
}
