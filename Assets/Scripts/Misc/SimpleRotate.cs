using System;
using UnityEngine;

namespace Assets.Scripts.Misc
{
    public class SimpleRotate : MonoBehaviour
    {
        [SerializeField] private Vector3 _rotationAxis = Vector3.up;
        [SerializeField] private float _rotationSpeed = 5;

        private void Update()
        {
            transform.Rotate(_rotationAxis, _rotationSpeed * Time.deltaTime);
        }
    }
}
