using Assets.Scripts.Utils.Math;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Input
{
    [CreateAssetMenu(fileName = "MouseDataSO", menuName = "Gameplay/Mouse Data")]
    public class MouseDataSO : ScriptableObject
    {
        public event UnityAction<Vector3> WorldPosEvent = delegate { };
        [SerializeField] private LayerMask _mouseInteractionLayers = default;

        private Vector3 _worldPos = Vector3.zero;
        public Vector3 WorldPos => _worldPos;

        private Vector2 _screenPos = Vector2.zero;

        public void UpdatePosition()
        {
            _worldPos = MouseMath.MouseWorldPosition(_screenPos, _mouseInteractionLayers);
            WorldPosEvent.Invoke(_worldPos);
        }
        
        public void SetScreenPos(Vector2 v)
        {
            _screenPos = v;
        }
    }
}
