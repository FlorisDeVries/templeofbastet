﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Input
{
    [CreateAssetMenu(fileName = "InputHandler", menuName = "Gameplay/Input Handler")]
    public class InputHandler : ScriptableObject, GameInput.IGameplayActions
    {
        public event UnityAction<Vector2> MoveEvent = delegate { };
        public event UnityAction<Vector2> MouseMoveEvent = delegate { };

        public event UnityAction<bool> ShootEvent = delegate { }; 
        public event UnityAction<bool> DashEvent = delegate { };

        private GameInput _gameInput;

        private void OnEnable()
        {
            if (_gameInput == null)
            {
                _gameInput = new GameInput();
                _gameInput.Gameplay.SetCallbacks(this);
            }

            // Enable desire input scheme
            _gameInput.Gameplay.Enable();
        }

        private void OnDisable()
        {
            // Disable all input schemes
            _gameInput.Gameplay.Disable();
        }

        #region EventInvokers
        public void OnMove(InputAction.CallbackContext context)
        {
            MoveEvent.Invoke(context.ReadValue<Vector2>());
        }

        public void OnDash(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                DashEvent.Invoke(true);
            }
            else if (context.canceled)
            {
                DashEvent.Invoke(false);
            }
        }

        public void OnMouseLook(InputAction.CallbackContext context)
        {
            MouseMoveEvent.Invoke(context.ReadValue<Vector2>());
        }

        public void OnShoot(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                ShootEvent.Invoke(true);
            }
            else if (context.canceled)
            {
                ShootEvent.Invoke(false);
            }
        }
        #endregion
    }
}
