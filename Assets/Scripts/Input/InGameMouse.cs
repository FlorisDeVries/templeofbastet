using System;
using Assets.Scripts.Utils.Math;
using UnityEngine;

namespace Assets.Scripts.Input
{
    public class InGameMouse : MonoBehaviour
    {
        [SerializeField] private MouseDataSO _mouseData = default;
        [SerializeField] private InputHandler _inputHandler = default;

        private void OnEnable()
        {
            _inputHandler.MouseMoveEvent += OnMouseMove;
        }

        private void OnDisable()
        {
            _inputHandler.MouseMoveEvent -= OnMouseMove;
        }

        private void FixedUpdate()
        {
            _mouseData.UpdatePosition();
            transform.position = _mouseData.WorldPos + new Vector3(0,0.3f,0);
        }

        private void OnMouseMove(Vector2 screenPos)
        {
            _mouseData.SetScreenPos(screenPos);
        }
    }
}
