namespace Assets.Scripts.GridExtensions
{
    public interface IBrushable
    {
        void OnUpdate();
        void OnErase();
        int GetLayer();
        TileType GetTileType();
    }
}
