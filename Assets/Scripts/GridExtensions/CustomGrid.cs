using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.GridExtensions
{
    public enum TileType
    {
        None,
        Floor,
        Pillar,
        Step,
        Trap,
        Invisible
    }

    public class CustomGrid : MonoBehaviour
    {
        [SerializeField] private Dictionary<Vector2Int, IBrushable> _tiles = new Dictionary<Vector2Int, IBrushable>();

        private Grid _grid;

        public Grid Grid
        {
            get
            {
                if (_grid != null)
                {
                    return _grid;
                }

                _grid = GetComponent<Grid>();
                return _grid;
            }
        }
        
        public void RegisterTile(Vector2Int pos, IBrushable tile)
        {
            if (!_tiles.ContainsKey(pos))
            {
                _tiles.Add(pos, tile);
            }
        }

        public void RemoveTile(Vector2Int pos)
        {
            _tiles.Remove(pos);
        }

        public Dictionary<Vector2Int, IBrushable> GetNeighbours(Vector2Int pos)
        {
            var neighbours = new Dictionary<Vector2Int, IBrushable>();

            if (GetTile(pos + new Vector2Int(1, 0)) != null)
                neighbours.Add(new Vector2Int(1, 0), GetTile(pos + new Vector2Int(1, 0)));

            if (GetTile(pos + new Vector2Int(-1, 0)) != null)
                neighbours.Add(new Vector2Int(-1, 0), GetTile(pos + new Vector2Int(-1, 0)));

            if (GetTile(pos + new Vector2Int(0, -1)) != null)
                neighbours.Add(new Vector2Int(0, -1), GetTile(pos + new Vector2Int(0, -1)));

            if (GetTile(pos + new Vector2Int(0, 1)) != null)
                neighbours.Add(new Vector2Int(0, 1), GetTile(pos + new Vector2Int(0, 1)));

            return neighbours;
        }

        private IBrushable GetTile(Vector2Int pos)
        {
            return _tiles.ContainsKey(pos) ? _tiles[pos] : null;
        }

        [Button("Update")]
        private void UpdateAll()
        {
            foreach (var brushable in _tiles)
            {
                brushable.Value.OnUpdate();
            }
        }
    }
}
