using System.Collections.Generic;
using Assets.Scripts.Environment;
using UnityEngine;

namespace Assets.Scripts.GridExtensions
{
    [ExecuteInEditMode]
    public abstract class ABrushable : MonoBehaviour, IBrushable
    {
        // Grid parameters for determining pattern
        protected CustomGrid grid;
        protected Vector2Int tilePosition;
        protected int layer = -999;
        protected TileType tileType = TileType.None;
        protected TileType prevTileType = TileType.None;
        protected Dictionary<Vector2Int, IBrushable> neighbours;

        protected virtual void OnEnable()
        {
            if (transform.position == Vector3.zero)
                return;

            if (!GetGridData())
                return;

            SetupTile();
            UpdateNeighbours();
        }

        protected virtual bool GetGridData()
        {
            grid = transform.GetComponentInParent<CustomGrid>();
            if (!grid)
            {
                Debug.LogWarning($"No grid was found in the grandparent of the trap");
                return false;
            }

            var tilePos = grid.Grid.WorldToCell(transform.localPosition);
            tilePosition = new Vector2Int(tilePos.x, tilePos.y);
            layer = (int)transform.position.y;

            grid.RegisterTile(tilePosition, this);
            neighbours = grid.GetNeighbours(tilePosition);
            return true;
        }

        protected abstract void SetupTile();

        protected virtual void UpdateNeighbours()
        {
            if (tileType != prevTileType)
            {
                prevTileType = tileType;
                foreach (var neighbour in neighbours)
                {
                    neighbour.Value.OnUpdate();
                }
            }
        }

        public virtual void OnUpdate()
        {
            if (!GetGridData())
                return;

            SetupTile();
            UpdateNeighbours();
        }

        public virtual void OnErase()
        {
            grid = transform.GetComponentInParent<CustomGrid>();
            grid.RemoveTile(tilePosition);

            //var neighbours = grid.GetNeighbours(tilePosition);
            //foreach (var neighbour in neighbours)
            //{
            //    neighbour.Value.OnUpdate();
            //}
        }

        public virtual int GetLayer()
        {
            return layer;
        }

        public virtual TileType GetTileType()
        {
            return tileType;
        }
    }
}
