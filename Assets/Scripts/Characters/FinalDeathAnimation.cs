using UnityEngine;

namespace Assets.Scripts.Characters
{
    public class FinalDeathAnimation : MonoBehaviour
    {
        private float _speed = 5;
    
        void FixedUpdate()
        {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, _speed * Time.fixedDeltaTime);

            if (Vector3.Distance(transform.localScale, Vector3.up) < .1f)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
