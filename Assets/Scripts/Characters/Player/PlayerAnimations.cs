using System;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using UnityEngine;

namespace Assets.Scripts.Characters.Player
{
    public class PlayerAnimations : MonoBehaviour
    {
        [SerializeField] private PlayerDataSO _playerData;

        private static readonly int Forward = Animator.StringToHash("Forward");
        private static readonly int Dashing = Animator.StringToHash("Dashing");
        private static readonly int AttackAnimation = Animator.StringToHash("AttackIndex");
        private Animator _animator = default;

        private void Animate()
        {
            // If no animator, return
            if (_animator == null)
                return;

            _animator.SetFloat(Forward, _playerData.MoveDirection.magnitude);
        }

        private void StartChannelDashAnimation()
        {
            _animator.SetBool(Dashing, true);
        }

        private void StartChannelAnimation()
        {
            _animator.SetFloat(AttackAnimation, _playerData.CurrentAbility.AttackIndex);
        }

        private void ResetToWalkingAnimation()
        {
            _animator.SetFloat(AttackAnimation, 0);
            _animator.SetBool(Dashing, false);
        }

        #region AnimationListeners
        public void OnDashStart()
        {
            _animator.SetBool(Dashing, false);
            _playerData.PlayerState.SetState(PlayerState.Dashing);
        }

        public void OnChannelingDone()
        {
            _playerData.PlayerState.SetState(PlayerState.UsingAbility);
        }

        public void OnAttackEnd()
        {
            _playerData.PlayerState.SetState(PlayerState.AbilityEnd);
        }
        #endregion

        private void OnEnable()
        {
            _animator = GetComponent<Animator>();

            _playerData.PlayerState.GetEventForState(PlayerState.ChannelingDash).AddListener(StartChannelDashAnimation);
            _playerData.PlayerState.GetEventForState(PlayerState.Channeling).AddListener(StartChannelAnimation);
            _playerData.PlayerState.GetEventForState(PlayerState.Walking).AddListener(ResetToWalkingAnimation);
        }

        private void Update()
        {
            Animate();
        }
    }
}
