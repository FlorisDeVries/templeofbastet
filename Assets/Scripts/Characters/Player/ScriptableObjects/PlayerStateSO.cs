using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Characters.Player.ScriptableObjects
{
    public enum PlayerState
    {
        Walking,
        ChannelingDash,
        Dashing,
        Channeling,
        UsingAbility,
        AbilityEnd
    }

    [CreateAssetMenu(fileName = "PlayerStateSO", menuName = "Characters/Player/Player State")]
    public class PlayerStateSO : ScriptableObject
    {
        private PlayerState _currentState = PlayerState.Walking;
        public PlayerState CurrentState => _currentState;

        private Dictionary<PlayerState, UnityEvent> _changeEvents = new Dictionary<PlayerState, UnityEvent>();

        public void SetState(PlayerState targetState)
        {
            switch (targetState)
            {
                case PlayerState.AbilityEnd:
                    // First reset to walking
                    _currentState = PlayerState.Walking;
                    _changeEvents[_currentState].Invoke();

                    // Call Ability end, allowing for delayed actions
                    _changeEvents[targetState].Invoke();
                    break;
                default:
                    _currentState = targetState;
                    _changeEvents[_currentState].Invoke();
                    break;
            }
        }

        public UnityEvent GetEventForState(PlayerState state)
        {
            return _changeEvents[state];
        }

        private void OnEnable()
        {
            Init();
        }

        private void OnDisable()
        {
            foreach (var changeEvent in _changeEvents.Values)
            {
                changeEvent.RemoveAllListeners();
            }
        }

        private void Init()
        {
            _currentState = PlayerState.Walking;

            _changeEvents = new Dictionary<PlayerState, UnityEvent>();
            foreach (var state in (PlayerState[])Enum.GetValues(typeof(PlayerState)))
            {
                _changeEvents.Add(state, new UnityEvent());
            }
        }
    }
}
