﻿using Assets.Scripts.Characters.Data;
using Assets.Scripts.Gameplay;
using Assets.Scripts.Gameplay.Combat;
using Assets.Scripts.Input;
using Assets.Scripts.ScriptableEvents;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Characters.Player.ScriptableObjects
{
    [CreateAssetMenu(fileName = "PlayerDataSO", menuName = "Characters/Player/Player Data")]
    public class PlayerDataSO : BasicStatsSO
    {
        // Player Stats: should be moved to PlayerStatsSO
        [Header("Player specific settings")]
        [SerializeField] private float _dashDistance = 25f;
        public float DashDistance => _dashDistance;
        [SerializeField] private float _dashDecay = 0.01f;
        public float DashDecay => _dashDecay;
        [SerializeField] private float _minDashVelocity = 10.0f;
        public float MinDashVelocity => _minDashVelocity;
        [SerializeField] private float _pickupRange = 0.0f;
        public float PickupRange => _pickupRange;


        [Header("Player shared components")]
        [SerializeField] private InputHandler _inputHandler = default;
        public InputHandler InputHandler => _inputHandler;

        [SerializeField] private GameplayManagerSO _gameplayManager = default;
        public GameplayManagerSO GameplayManager => _gameplayManager;

        [SerializeField] private CameraEventsSO _cameraEvents = default;
        public CameraEventsSO CameraEvents => _cameraEvents;

        [SerializeField] private PlayerStateSO _playerState = default;
        public PlayerStateSO PlayerState => _playerState;

        [SerializeField] private MouseDataSO _mouseData = default;
        public MouseDataSO MouseData => _mouseData;

        // Current DATA
        public Vector3 MoveDirection { get; set; }
        public float CurrentHealth { get; private set; } = 0;
        public Transform ThirdPersonFollowTransform { get; set; }
        public Vector3 PlayerPos { get; set; }
        public AbilitySO CurrentAbility { get; set; }

        // PlayerDataEvents
        public event UnityAction<float> PlayerHpUpdateEvent = delegate { };
        public event UnityAction PlayerDeathEvent = delegate { };

        private void OnEnable()
        {
            Reset();
        }

        private void Reset()
        {
            CurrentHealth = maxHealth;
        }

        public void DoDamage(float damage)
        {
            CurrentHealth -= damage;
            if (CurrentHealth <= 0)
            {
                PlayerDeathEvent.Invoke();
                _gameplayManager.GameOver();
            }

            PlayerHpUpdateEvent.Invoke(damage);
        }

        public bool InPickupRange(Vector3 pos)
        {
            return (PlayerPos - pos).magnitude <= _pickupRange;
        }
    }
}
