using System;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using Assets.Scripts.Gameplay;
using Assets.Scripts.Gameplay.Combat;
using Assets.Scripts.Utils.UnityExtensions;
using UnityEngine;

namespace Assets.Scripts.Characters.Player
{
    public class PlayerCombat : MonoBehaviour, IDamageable
    {
        [SerializeField] private PlayerDataSO _playerData;
        [SerializeField] private Faction _faction = default;
        [SerializeField] private AbilitySO _defaultAbility = default;
        [SerializeField] private float _fireCoyote = .1f;

        private bool _firing = false;
        private float _firingTimer = 0f;

        public void TakeDamage(float damage)
        {
            // Damage effects
            _playerData.CameraEvents.CameraShakeEvent.Invoke(damage / 10, damage / 100);

            // Game logic
            _playerData.DoDamage(damage);
        }

        public void OnAttack(bool buttonPressed)
        {
            // Button timer part
            if (!buttonPressed)
            {
                _firingTimer = _fireCoyote;
                return;
            }
            _firing = true;

            // Normal logic part
            Attack();
        }

        private void Attack()
        {
            if (_playerData.PlayerState.CurrentState != PlayerState.Walking)
            {
                return;
            }
            _playerData.PlayerState.SetState(PlayerState.Channeling);
        }

        public void OnUseAbility()
        {
            Instantiate(_playerData.CurrentAbility.AttackPrefab, transform.position + Vector3.up, Quaternion.LookRotation(transform.forward))
                .GetInterface<IAttack>().SetupAttack(_faction, _playerData.CurrentAbility);
        }

        public void OnAbilityEnd()
        {
            if (_firing)
                Attack();
        }

        private void OnEnable()
        {
            _playerData.CurrentAbility = _defaultAbility;

            _playerData.InputHandler.ShootEvent += OnAttack;
            _playerData.PlayerState.GetEventForState(PlayerState.UsingAbility).AddListener(OnUseAbility);
            _playerData.PlayerState.GetEventForState(PlayerState.AbilityEnd).AddListener(OnAbilityEnd);
        }

        private void OnDisable()
        {
            _playerData.InputHandler.ShootEvent -= OnAttack;
        }

        private void FixedUpdate()
        {
            if (_firingTimer <= 0)
                return;

            _firingTimer -= Time.fixedDeltaTime;
            if (_firingTimer <= 0)
                _firing = false;
        }
    }
}
