using System;
using System.Collections;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using UnityEngine;
using UnityEngine.VFX;

namespace Assets.Scripts.Characters.Player
{
    public class PlayerVFX : MonoBehaviour
    {
        [SerializeField] private PlayerDataSO _playerData;

        [Header("Walking puffs")]
        [SerializeField] private VisualEffect _walkEffect = default;
        [SerializeField] private float _puffInterval = .5f;
        [SerializeField] private float _puffSensitivity = .5f;

        [Header("Other Effects")]
        [SerializeField] private VisualEffect _dashEffect = default;

        [SerializeField] private VisualEffect _damageEffect = default;
        private SkinnedMeshRenderer _skinnedMesh = default;

        // Puff effect parameters
        private bool _walking = false;

        // ReSharper disable once FunctionRecursiveOnAllPaths
        private IEnumerator WalkingPuffs()
        {
            yield return new WaitForSeconds(_puffInterval);
            if (_playerData.PlayerState.CurrentState == PlayerState.Walking && _walking) 
                _walkEffect.Play();

            StartCoroutine(WalkingPuffs());
        }

        private void OnTakeDamage(float damage)
        {
            var m = new Mesh();
            _skinnedMesh.BakeMesh(m);
            var m2 = new Mesh { vertices = m.vertices };

            _damageEffect.SetMesh("TargetMesh", m2);
            _damageEffect.Play();
        }

        private void Update()
        {
            // Set effects parameters
            _walking = (_playerData.MoveDirection.magnitude > _puffSensitivity);
        }

        private void OnEnable()
        {
            _walking = false;
            StartCoroutine(WalkingPuffs());

            _playerData.PlayerState.GetEventForState(PlayerState.Dashing).AddListener(() => _dashEffect.Play());

            _skinnedMesh = GetComponentInChildren<SkinnedMeshRenderer>();
            _playerData.PlayerHpUpdateEvent += OnTakeDamage;
        }
    }
}
