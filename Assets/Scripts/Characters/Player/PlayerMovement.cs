using Assets.Scripts.Characters.Data;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using Assets.Scripts.Utils.UnityExtensions;
using UnityEngine;

namespace Assets.Scripts.Characters.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        // Player specifics
        [SerializeField] private PlayerDataSO _playerData;

        [Header("Wall clipping prevention/Ground detection")]
        [SerializeField]
        private LayerMask _collisionLayerMask = default;
        [SerializeField] private float _collisionOffset = .25f;
        private Vector3 CollisionDetectionOffset => new Vector3(0, _collisionOffset, 0);
        [SerializeField] private float _slopeForce = 1f;

        // Movement variables 
        private Vector3 _dashVelocity;
        private Rigidbody _rigidbody;
        [SerializeField] private float _dashCoyoteTime = .1f;

        private bool _dashing = false;
        private float _dashingTimer = 0f;

        private void UpdateRotation()
        {
            var lookDirection = _playerData.MoveDirection;
            if (_playerData.PlayerState.CurrentState != PlayerState.Walking && _playerData.PlayerState.CurrentState != PlayerState.Dashing && _playerData.PlayerState.CurrentState != PlayerState.ChannelingDash)
            {
                // While not walking look at mouse
                lookDirection = _playerData.MouseData.WorldPos - transform.position;
            }

            // If the two vectors are in opposite directions, rotate the forward slightly so the character does not get stuck on rotation
            if (transform.forward == lookDirection * -1)
                transform.forward = Quaternion.Euler(0, 5f, 0) * transform.forward;

            if (lookDirection != Vector3.zero)
                transform.forward =
                    Vector3.Lerp(transform.forward, lookDirection, _playerData.RotationSpeed * Time.fixedDeltaTime);
        }

        private void Dash()
        {
            if (_playerData.PlayerState.CurrentState != PlayerState.Walking) return;

            _playerData.PlayerState.SetState(PlayerState.ChannelingDash);

            var dash = new Vector3(1, 0, 1);

            var forward = _playerData.MoveDirection != Vector3.zero ? _playerData.MoveDirection : transform.forward;
            _dashVelocity = Vector3.Scale(forward, _playerData.DashDistance * dash);
        }

        private void StopDashing()
        {
            _dashVelocity = Vector3.zero;
            _playerData.PlayerState.SetState(PlayerState.AbilityEnd);
        }
        private bool OnSlope()
        {
            if (Physics.Raycast(transform.position, Vector3.down, out var slopeHit, _collisionOffset, _collisionLayerMask))
            {
                return slopeHit.normal != Vector3.up;
            }

            return false;
        }

        private bool OnGround()
        {
            return Physics.CheckSphere(transform.position, _collisionOffset, _collisionLayerMask);
        }

        private void Move()
        {
            if (_playerData.PlayerState.CurrentState == PlayerState.ChannelingDash)
                return;

            var speed = _playerData.MovementSpeed;
            if (_playerData.PlayerState.CurrentState != PlayerState.Walking)
            {
                speed /= 2;
            }

            // Movement logic (Movement/Physics)
            // Move logic
            var targetPosition = _rigidbody.position + _playerData.MoveDirection * (speed * Time.fixedDeltaTime);

            // Dash logic
            if (_dashVelocity != Vector3.zero)
            {
                targetPosition += _dashVelocity * Time.fixedDeltaTime;
                _dashVelocity = Vector3.Lerp(_dashVelocity, Vector3.zero, _playerData.DashDecay * Time.fixedDeltaTime);

                if (_dashVelocity.magnitude < _playerData.MinDashVelocity)
                {
                    StopDashing();
                }
            }

            // Move the character, but first check if we can move in direction
            var direction = targetPosition - _rigidbody.position;
            var collisionRay = new Ray(_rigidbody.position + CollisionDetectionOffset, direction);
            if (!Physics.Raycast(collisionRay, out _, direction.magnitude, _collisionLayerMask))
            {
                _rigidbody.MovePosition(targetPosition);
            }

            if (OnSlope())
            {
                _rigidbody.AddForce(Vector3.down * Time.fixedDeltaTime * _slopeForce);
            }

            _playerData.PlayerPos = transform.position;
        }

        private void DashTimer()
        {
            if (_dashingTimer <= 0)
                return;

            _dashingTimer -= Time.fixedDeltaTime;
            if (_dashingTimer <= 0)
                _dashing = false;
        }

        #region MonoBehaviour
        private void FixedUpdate()
        {
            UpdateRotation();
            Move();
            DashTimer();
        }
        #endregion

        // Setup listeners for input
        private void OnEnable()
        {
            // Get Components
            _rigidbody = GetComponent<Rigidbody>();

            // Add listeners
            _playerData.InputHandler.MoveEvent += OnMove;
            _playerData.InputHandler.DashEvent += OnDash;

            _playerData.PlayerState.GetEventForState(PlayerState.AbilityEnd).AddListener(OnAbilityEnd);
        }

        // Remove all listeners for input
        private void OnDisable()
        {
            // Reset listeners
            _playerData.InputHandler.MoveEvent -= OnMove;
            _playerData.InputHandler.DashEvent -= OnDash;
        }

        #region ListenerEvents
        private void OnMove(Vector2 moveVector2)
        {
            var cameraRotation = UnityEngine.Camera.main.transform.rotation;
            moveVector2 = moveVector2.Rotate(cameraRotation.eulerAngles.y - 180);
            _playerData.MoveDirection = new Vector3(
                moveVector2.x,
                0,
                moveVector2.y
            );
        }

        private void OnDash(bool buttonPressed)
        {
            // Button timer part
            if (!buttonPressed)
            {
                _dashingTimer = _dashCoyoteTime;
                return;
            }
            _dashing = true;

            // Normal logic part
            Dash();
        }

        public void OnAbilityEnd()
        {
            if (_dashing)
                Dash();
        }
        #endregion
    }
}
