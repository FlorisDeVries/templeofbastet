using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Characters.NPC
{
    public class AnimationListener : MonoBehaviour
    {
        public UnityAction FireEvent = delegate { };
        public UnityAction AttackEndEvent = delegate { };
        public UnityAction FinalDeathEvent = delegate { };
        public UnityAction DashEvent = delegate { };
        
        public void OnChannelingDone()
        {
            FireEvent.Invoke();
        }

        public void OnAttackEnd()
        {
            AttackEndEvent.Invoke();
        }

        public void OnFinalDeath()
        {
            FinalDeathEvent.Invoke();
        }
    }
}
