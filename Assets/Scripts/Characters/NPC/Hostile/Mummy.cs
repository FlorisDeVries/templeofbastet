using System;
using System.Collections;
using Assets.Scripts.Characters.Data;
using Assets.Scripts.Characters.Player;
using Assets.Scripts.Gameplay;
using Assets.Scripts.Gameplay.Combat;
using Assets.Scripts.Gameplay.Objectives;
using Assets.Scripts.UI.CharacterStats;
using Assets.Scripts.Utils.UnityExtensions;
using UnityEngine;

namespace Assets.Scripts.Characters.NPC.Hostile
{
    public class Mummy : MonoBehaviour, IDamageable
    {
        [SerializeField] private SoulObjective _soulObjective = default;
        [SerializeField] private EnemyStatsSO _stats = default;
        [SerializeField] private AbilitySO _defaultAbility = default;
        [SerializeField] private Faction _faction = default;

        // FSM
        private NpcState _state = NpcState.Idle;
        private bool _attackOnCooldown = false;

        // Movement variables
        private CharacterController _characterController = default;

        // Player interactions
        private PlayerCombat _player = null;

        // Animation variables
        private AnimationListener _animationListener;

        // Combat
        private float _currentHealth;
        private HpBar _hpBar;
        private float _staggerTimer = 0;

        private void FixedUpdate()
        {
            switch (_state)
            {
                case NpcState.Idle:
                    DetectEnemy();
                    break;
                case NpcState.Chasing:
                    Move();
                    DetectEnemy();
                    break;
                case NpcState.InRange:
                    Attack();
                    break;
                case NpcState.Attacking:
                    break;
                case NpcState.Death:
                    break;
                case NpcState.Stagger:
                    _staggerTimer -= Time.fixedDeltaTime;
                    if (_staggerTimer <= 0)
                    {
                        _state = NpcState.Idle;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void DetectEnemy()
        {

            LayerMask mask = LayerMask.GetMask($"Player");
            var colliders = Physics.OverlapSphere(transform.position, _stats.VisionRange, mask);

            if (colliders.Length < 1)
            {
                _characterController.StopMoving();
                _state = NpcState.Idle;
                return;
            }

            // Found player inside range, check if player is inside attack sphere
            _player = colliders[0].GetComponent<PlayerCombat>();

            if (Physics.Linecast(transform.position, _player.transform.position, _stats.VisionMask))
            {
                // Something in the way
                return;
            }

            colliders = Physics.OverlapSphere(transform.position + transform.forward.normalized * _defaultAbility.Range/ 2, _defaultAbility.Range / 2, mask);
            _state = (colliders.Length > 0 && !_attackOnCooldown) ? NpcState.InRange : NpcState.Chasing;
        }

        private void Move()
        {
            _characterController.MoveTowards(_player.transform.position, _defaultAbility.Range);
        }

        private void Attack()
        {
            if (_attackOnCooldown)
                return;

            _attackOnCooldown = true;
            _characterController.Attack(_defaultAbility.AttackIndex);
            _state = NpcState.Attacking;
        }

        private void DealDamage()
        {
            Instantiate(_defaultAbility.AttackPrefab, transform.position + Vector3.up, Quaternion.LookRotation(transform.forward))
                .GetInterface<IAttack>().SetupAttack(_faction, _defaultAbility);
        }

        private void EndAttack()
        {
            StartCoroutine(AttackCooldown());
            _characterController.CancelAttack();
            _state = NpcState.Idle;
        }

        public void TakeDamage(float damage)
        {
            if(_state == NpcState.Death)
                return;

            _currentHealth -= damage;
            _hpBar.HealthUpdate(_currentHealth);
            if (_currentHealth <= 0)
            {
                Die();
                return;
            }

            _characterController.TakeDamage();
            _state = NpcState.Stagger;
            _staggerTimer = _stats.StaggerTime;
            StartCoroutine(AttackCooldown());
        }

        private void Die()
        {
            _soulObjective.SpawnSouls(transform.position);
            _hpBar.OnDeath();
            gameObject.layer = (int) UnityLayers.VFX;
            _characterController.Die();
            _state = NpcState.Death;
        }

        private void FinalDeath()
        {
            gameObject.AddComponent<FinalDeathAnimation>();
        }

        private IEnumerator AttackCooldown()
        {
            yield return new WaitForSeconds(_defaultAbility.Cooldown);
            _attackOnCooldown = false;
        }

        private void OnEnable()
        {
            _characterController = GetComponent<CharacterController>();
            _animationListener = GetComponentInChildren<AnimationListener>();
            _hpBar = GetComponentInChildren<HpBar>();

            _characterController.CharacterStats = _stats;
            _currentHealth = _stats.MaxHP;
            _hpBar.Setup(_stats);

            // Add listeners
            _animationListener.FireEvent += DealDamage;
            _animationListener.AttackEndEvent += EndAttack;
            _animationListener.FinalDeathEvent += FinalDeath;
        }

        private void OnDisable()
        {
            // Remove listeners
            _animationListener.FireEvent -= DealDamage;
            _animationListener.AttackEndEvent -= EndAttack;
            _animationListener.FinalDeathEvent -= FinalDeath;
        }

        private void OnDrawGizmosSelected()
        {
            // Detection debug
            Gizmos.color = _player ? Color.yellow : Color.green;
            Gizmos.DrawWireSphere(transform.position, _stats.VisionRange);

            _defaultAbility.DrawGizmo(transform);
        }
    }
}
