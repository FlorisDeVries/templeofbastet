using UnityEngine;

namespace Assets.Scripts.Characters.NPC
{
    public enum NpcState
    {
        Idle,
        Chasing,
        InRange,
        Attacking,
        Death,
        Stagger
    }
}
