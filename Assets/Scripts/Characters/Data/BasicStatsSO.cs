using UnityEngine;

namespace Assets.Scripts.Characters.Data
{
    [CreateAssetMenu(fileName = "BasicStatsSO", menuName = "Characters/Basic Stats")]
    public class BasicStatsSO : ScriptableObject
    {
        [Header("Combat Stats")]
        [SerializeField] protected float maxHealth;
        public float MaxHealth => maxHealth;

        [Header("Movement settings")]
        [SerializeField] protected float movementSpeed = 5f;
        public float MovementSpeed => movementSpeed;
        [SerializeField] protected float rotationSpeed = .1f;
        public float RotationSpeed => rotationSpeed;
    }
}
