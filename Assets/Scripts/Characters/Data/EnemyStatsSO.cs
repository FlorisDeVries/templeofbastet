using UnityEngine;

namespace Assets.Scripts.Characters.Data
{
    [CreateAssetMenu(fileName = "EnemyStatsSO", menuName = "Characters/Enemy Stats")]
    public class EnemyStatsSO : BasicStatsSO
    {
        [Header("Enemy specific settings")]
        [SerializeField] private float _visionRange = 10f;
        public float VisionRange => _visionRange;
        [SerializeField] private LayerMask _visionMask = default;
        public LayerMask VisionMask => _visionMask;
        [SerializeField] private float _maxHp = 50f;
        public float MaxHP => _maxHp;
        [SerializeField] private float _staggerTime = 2f;
        public float StaggerTime => _staggerTime;
    }
}
