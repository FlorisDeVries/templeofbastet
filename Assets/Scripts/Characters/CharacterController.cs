using System;
using System.Diagnostics.Eventing.Reader;
using Assets.Scripts.Characters.Data;
using UnityEngine;

namespace Assets.Scripts.Characters
{
    public class CharacterController : MonoBehaviour
    {
        [HideInInspector]
        public BasicStatsSO CharacterStats = default;

        [Header("Collision prevention")]
        [SerializeField] private LayerMask _collisionLayerMask = default;
        [SerializeField] private float _collisionRayHeight = .25f;

        // Movement variables
        protected new Rigidbody rigidbody;
        [SerializeField] protected Vector3 moveDirection;

        // Animation variables
        protected Animator animator;
        private static readonly int ForwardAnimation = Animator.StringToHash("Forward");
        private static readonly int DamageAnimation = Animator.StringToHash("Damage");
        private static readonly int DeathAnimation = Animator.StringToHash("Death");
        private static readonly int AttackAnimation = Animator.StringToHash("AttackIndex");

        // Collision detection parameters
        private Vector3 CollisionDetectionOffset => new Vector3(0, _collisionRayHeight, 0);

        protected virtual void UpdateRotation()
        {
            if (moveDirection == Vector3.zero)
                return;

            // If the two vectors are in opposite directions, rotate the forward slightly so the character does not get stuck on rotation
            if (transform.forward == moveDirection * -1)
                transform.forward = Quaternion.Euler(0, 5f, 0) * transform.forward;

            transform.forward = Vector3.Lerp(transform.forward, moveDirection, CharacterStats.RotationSpeed);

            if (!(Vector3.Distance(transform.forward, moveDirection) < .5f)) return;

            transform.forward = moveDirection;
            moveDirection = Vector3.zero;
        }

        #region MonoBehaviour
        public void MoveTowards(Vector3 target, float minDist)
        {
            moveDirection = target - transform.position;
            animator.SetFloat(ForwardAnimation, 0.0f);
            if (!(moveDirection.magnitude > minDist)) return;

            // Really basic forward animation
            animator.SetFloat(ForwardAnimation, 1.0f);

            // Calculate movement
            moveDirection = moveDirection.normalized;
            var targetPosition = rigidbody.position + moveDirection * (CharacterStats.MovementSpeed * Time.fixedDeltaTime);

            // Move the character, but first check if we can move in direction
            var direction = targetPosition - rigidbody.position;
            var collisionRay = new Ray(rigidbody.position + CollisionDetectionOffset, direction);
            if (!Physics.Raycast(collisionRay, out _, direction.magnitude, _collisionLayerMask))
            {
                rigidbody.MovePosition(targetPosition);
            }
        }

        public void StopMoving()
        {
            // Really basic forward animation
            animator.SetFloat(ForwardAnimation, 0.0f);
        }

        public void Attack(int attackIndex)
        {
            StopMoving();
            animator.SetFloat(AttackAnimation, attackIndex);
        }

        public void CancelAttack()
        {
            animator.SetFloat(AttackAnimation, 0);
        }

        public void TakeDamage()
        {
            StopMoving();
            animator.SetTrigger(DamageAnimation);
        }

        public void Die()
        {
            StopMoving();
            animator.SetTrigger(DeathAnimation);
        }

        protected virtual void Update()
        {
            // Visual logic (rotation/animation/registering input)
            UpdateRotation();
        }

        protected virtual void OnEnable()
        {
            rigidbody = GetComponent<Rigidbody>();
            animator = GetComponentInChildren<Animator>();
        }
        #endregion
    }
}
