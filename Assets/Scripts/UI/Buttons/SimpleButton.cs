using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UI.Buttons
{
    public class SimpleButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler,
        IPointerUpHandler
    {
        [Header("UI elements")] 
        [SerializeField] private GameObject _plainImage = default;

        [SerializeField] private GameObject _hoverImage = default;
        [SerializeField] private GameObject _pressedImage = default;

        [SerializeField] private TMP_Text _plainText = null;
        [SerializeField] private TMP_Text _hoverText = null;
        [SerializeField] private TMP_Text _pressedText = null;

        [Header("Settings")] 
        [SerializeField] private string _buttonText = "";

        public UnityAction OnClick = delegate { };

        private ButtonState _state = ButtonState.None;

        private void OnEnable()
        {
            SetState(ButtonState.None);
        }

        private void OnValidate()
        {
            if (_plainText)
                _plainText.text = _buttonText;
            if (_hoverText)
                _hoverText.text = _buttonText;
            if (_pressedText)
                _pressedText.text = _buttonText;
        }

        private void SetState(ButtonState newState)
        {
            _state = newState;
            _hoverImage.SetActive(_state == ButtonState.Hovered);
            _plainImage.SetActive(_state == ButtonState.None);
            _pressedImage.SetActive(_state == ButtonState.Pressed);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            SetState(ButtonState.Hovered);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            SetState(ButtonState.None);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            SetState(ButtonState.Pressed);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_state == ButtonState.None)
                return;
            
            if (_state == ButtonState.Pressed)
                OnClick.Invoke();
            
            SetState(ButtonState.Hovered);
        }
    }
}