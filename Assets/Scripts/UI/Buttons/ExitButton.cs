﻿using UnityEditor;
using UnityEngine;

namespace UI.Buttons
{
    [RequireComponent(typeof(SimpleButton))]
    public class ExitButton : MonoBehaviour
    {
        private SimpleButton _button = default;

        private void OnEnable()
        {
            _button = GetComponent<SimpleButton>();
            _button.OnClick += OnExit;
        }

        private void OnExit()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
        }
    }
}