﻿namespace UI.Buttons
{
    public enum ButtonState
    {
        None,
        Hovered,
        Pressed
    }
}