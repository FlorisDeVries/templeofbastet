using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.Buttons
{
    [RequireComponent(typeof(SimpleButton))]
    public class PlayButton : MonoBehaviour
    {
        private SimpleButton _button = default;

        private void OnEnable()
        {
            _button = GetComponent<SimpleButton>();
            _button.OnClick += OnPlay;
        }

        private void OnPlay()
        {
            // TODO: Make some scene manager
            SceneManager.LoadScene("Scenes/Test/RoomDesigner");
        }
    }
}
