using System;
using UnityEngine;

namespace Assets.Scripts.UI.Canvas
{
    public class WorldSpaceCanvas : MonoBehaviour
    {
        private Vector3 _rotation = Vector3.zero;
        private void OnEnable()
        {
            var cam = UnityEngine.Camera.main;
            if (cam == null)
            {
                this.enabled = false;
                return;
            }

            _rotation = transform.rotation.eulerAngles;
            _rotation.y = cam.transform.rotation.eulerAngles.y;
            _rotation.x = cam.transform.rotation.eulerAngles.x;
            transform.rotation = Quaternion.Euler(_rotation);
        }

        private void LateUpdate()
        {
            transform.rotation = Quaternion.Euler(_rotation);
        }
    }
}
