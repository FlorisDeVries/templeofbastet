using System;
using System.Collections.Generic;
using Assets.Scripts.Gameplay;
using UnityEngine;

namespace Assets.Scripts.UI.Canvas
{
    public class CanvasController : MonoBehaviour
    {
        [SerializeField] private GameplayManagerSO _gameplayManager = default;

        [SerializeField] private List<GameState> _displayState;
        private UnityEngine.Canvas _canvas;

        private void OnEnable()
        {
            _canvas = GetComponent<UnityEngine.Canvas>();
            _canvas.enabled = _displayState.Contains(_gameplayManager.CurrentGameState);
            _gameplayManager.GameStateChangeEvent += OnGameStateChanged;
        }

        private void OnDisable()
        {
            _gameplayManager.GameStateChangeEvent += OnGameStateChanged;
        }

        private void OnGameStateChanged(GameState newState)
        {
            _canvas.enabled = _displayState.Contains(newState);
        }
    }
}
