using System;
using Assets.Scripts.Gameplay.Objectives;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI.CharacterStats
{
    public class SoulCount : MonoBehaviour
    {
        [SerializeField] private SoulObjective _soulObjective = default;
        [SerializeField] private TMP_Text _text = default;

        private void OnEnable()
        {
            _text.text = _soulObjective.CollectedSouls.ToString();
            _soulObjective.SoulCollectedEvent += OnSoulUpdate;
        }

        private void OnSoulUpdate(float newCount)
        {
            _text.text = newCount.ToString();
        }
    }
}
