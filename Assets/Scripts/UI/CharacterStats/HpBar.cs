using System.Collections;
using Assets.Scripts.Characters.Data;
using UnityEngine;

namespace Assets.Scripts.UI.CharacterStats
{
    public class HpBar : MonoBehaviour
    {
        [Header("UIElements")]
        [SerializeField] private RectTransform _healthBar = default;
        [SerializeField] private RectTransform _delayBar = default;

        [Header("HpBar behaviour")]
        [SerializeField] private float _burstDelay = 2f;
        [SerializeField] private float _healthSpeed = .1f;
        [SerializeField] private float _burstSpeed = .01f;

        private bool _bursting = false;
        private float _burstTimer = 0f;

        private float _maxHp = 150;
        private float _targetHpRatio = 1;
        private float _targetDelayRatio = 1;

        private float _maxWidth = 150;
        private RectTransform _transform;

        public void HealthUpdate(float currentHp)
        {
            _targetHpRatio = currentHp / _maxHp;

            _burstTimer = _burstDelay;
            _bursting = true;
        }

        private void FixedUpdate()
        {
            _maxWidth = _transform.rect.width;
            UpdateBar(_healthBar, _targetHpRatio, _healthSpeed);
            UpdateBar(_delayBar, _targetDelayRatio, _burstSpeed);

            if (!_bursting) return;

            _burstTimer -= Time.fixedDeltaTime;

            if (!(_burstTimer <= 0)) return;

            _targetDelayRatio = _targetHpRatio;
            _bursting = false;
        }

        private void UpdateBar(RectTransform rect, float ratio, float speed)
        {
            var targetWidth = (1 - ratio) * -_maxWidth;

            var newWidth = Mathf.MoveTowards(rect.sizeDelta.x, targetWidth, speed * Time.fixedDeltaTime);
            rect.SetWidth(newWidth);
        }

        public void Setup(BasicStatsSO stats)
        {
            _transform = GetComponent<RectTransform>();

            _maxHp = stats.MaxHealth;
            _targetHpRatio = 1;
            _targetDelayRatio = 1;
        }

        public void OnDeath()
        {
            _targetHpRatio = 0;
            _targetDelayRatio = 0;
        }
    }
}
