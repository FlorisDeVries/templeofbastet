﻿using System;
using System.Collections;
using Assets.Scripts.Characters.Data;
using Assets.Scripts.Characters.Player;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.CharacterStats
{
    public class PlayerHpBar : HpBar
    {
        [Header("ScriptableObjects")]
        [SerializeField] private PlayerDataSO _playerData = default;
        
        private void OnEnable()
        {
            Setup(_playerData);

            _playerData.PlayerHpUpdateEvent += (_) =>
            {
                HealthUpdate(_playerData.CurrentHealth);
            };
            _playerData.PlayerDeathEvent += OnDeath;
        }

        private void OnDisable()
        {
            _playerData.PlayerHpUpdateEvent -= HealthUpdate;
            _playerData.PlayerDeathEvent -= OnDeath;
        }
    }
}
