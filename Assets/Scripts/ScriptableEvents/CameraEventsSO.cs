﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.ScriptableEvents
{
    [CreateAssetMenu(fileName = "CameraEventsSO", menuName = "Environment/CameraEventsSO")]
    public class CameraEventsSO : ScriptableObject
    {
        /// <summary>
        /// Event used to make the camera shake.
        /// arg0: Intensity of the shake
        /// arg1: Duration of the shake
        /// </summary>
        public UnityAction<float, float> CameraShakeEvent = delegate { };
    }
}
