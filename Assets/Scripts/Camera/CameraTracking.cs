﻿using Assets.Scripts.Characters.Data;
using Assets.Scripts.Characters.Player;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using Cinemachine;
using UnityEngine;

namespace Assets.Scripts.Camera
{
    /// <summary>
    /// Camera Follow class to be in the same scene as the CineMachine camera
    /// </summary>
    public class CameraTracking : MonoBehaviour
    {
        [SerializeField] private PlayerDataSO _playerData = default;
        [SerializeField] private Vector3 _offset = default;
        [SerializeField] private bool _copyRotation = false;

        [Header("Camera Distance")]
        [SerializeField] private CinemachineVirtualCamera _vCam = default;
        [SerializeField] private Vector2 _minMaxCameraDistance = new Vector2(1, 10);

        private void FixedUpdate()
        {
            var playerTransform = transform;
            playerTransform.position = _playerData.ThirdPersonFollowTransform.position + _offset;

            if (_copyRotation)
            {
                playerTransform.rotation = _playerData.ThirdPersonFollowTransform.rotation;
            }

            // Change VCam distance based on the follow angle
            var angle = playerTransform.localEulerAngles.x;
            var followDist = Mathf.Lerp(_minMaxCameraDistance.x, _minMaxCameraDistance.y, (angle - 15) / 60);

            var thirdPersonFollow = _vCam.GetCinemachineComponent<Cinemachine3rdPersonFollow>();
            if (thirdPersonFollow)
            {
                thirdPersonFollow.CameraDistance = followDist;
            }
        }
    }
}
