﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.ScriptableEvents;
using Cinemachine;
using UnityEngine;

namespace Assets.Scripts.Camera
{
    public class CameraShaker : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera _vCam = default;
        private CinemachineBasicMultiChannelPerlin _vCamNoise = default;

        [SerializeField] private CameraEventsSO _cameraEvents = default;

        // Key is intensity, value is duration
        private Dictionary<float, float> _shakes;

        private void ShakeCamera(float intensity, float duration)
        {
            if (_shakes.ContainsKey(intensity))
                _shakes[intensity] = duration;
            else
                _shakes.Add(intensity, duration);
        }

        private void FixedUpdate()
        {
            if (_shakes.Count == 0)
            {
                _vCamNoise.m_AmplitudeGain = 0;
                return;
            }

            // Get highest intensity
            var sorted = _shakes.Keys.ToList();
            sorted.Sort();

            var last = sorted.Last();
            _vCamNoise.m_AmplitudeGain = last;

            // Countdown timers and remove shakes
            var toRemove = new List<float>();
            foreach (var key in sorted)
            {
                _shakes[key] -= Time.fixedDeltaTime;
                if (_shakes[key] <= 0) toRemove.Add(key);
            }

            foreach (var f in toRemove) _shakes.Remove(f);
        }

        private void OnEnable()
        {
            _shakes = new Dictionary<float, float>();
            _cameraEvents.CameraShakeEvent += ShakeCamera;
            _vCamNoise = _vCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        private void OnDisable()
        {
            _cameraEvents.CameraShakeEvent -= ShakeCamera;
        }
    }
}
