using System;
using Assets.Scripts.Characters.Data;
using Assets.Scripts.Characters.Player;
using Assets.Scripts.Characters.Player.ScriptableObjects;
using UnityEngine;

namespace Assets.Scripts.Camera
{
    public class CameraTrackTarget : MonoBehaviour
    {

        [Header("Scriptable objects")]
        [SerializeField] private PlayerDataSO _playerData = default;

        private void OnEnable()
        {
            _playerData.ThirdPersonFollowTransform = this.transform;
        }
    }
}
